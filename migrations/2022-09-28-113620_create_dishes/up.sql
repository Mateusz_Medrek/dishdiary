-- Your SQL goes here

CREATE TABLE dishes (
  id INTEGER NOT NULL PRIMARY KEY,
  title VARCHAR NOT NULL,
  description TEXT NOT NULL
)
