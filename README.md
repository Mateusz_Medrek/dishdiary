# DishDiary

## Manage the database with diesel_cli
Note: All of those commands are getting executed from the project root folder

1. Install the diesel_cli tool
```sh
cargo install diesel_cli
```

2. Create empty database file
```sh
touch database.db
```

3. Create new migration
```sh
diesel migration generate <migration-name> --database-url=./database.db
```

4. List all migrations
```sh
diesel migration list --database-url=./database.db
```

5. Run the migrations (onto the database.db file)
```sh
diesel migration run --database-url=./database.db
```

