pub static VERSION: &str = "0.1.0";
pub static GETTEXT_PACKAGE: &str = "dishdiary";
pub static LOCALEDIR: &str = "/app/share/locale";
pub static PKGDATADIR: &str = "/app/share/dishdiary";
pub static APP_ID: &str = "dev.mateusz1913.dishdiary";
pub static NAME: &str = "DishDiary";
