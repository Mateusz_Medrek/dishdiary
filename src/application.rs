use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::clone;
use gtk::{gdk, gio, glib};

// use crate::config::APP_ID;
use crate::config::VERSION;
use crate::DishdiaryWindow;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct DishdiaryApplication {}

    #[glib::object_subclass]
    impl ObjectSubclass for DishdiaryApplication {
        const NAME: &'static str = "DishdiaryApplication";
        type Type = super::DishdiaryApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for DishdiaryApplication {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.setup_gactions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
        }
    }

    impl ApplicationImpl for DishdiaryApplication {
        // We connect to the activate callback to create a window when the application
        // has been launched. Additionally, this callback notifies us when the user
        // tries to launch a "second instance" of the application. When they try
        // to do that, we'll just present any existing window.
        fn activate(&self, application: &Self::Type) {
            // Get the current window or create one if necessary
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                // Set resource base path to data/resource/ directory
                application.set_resource_base_path(Some("/dev/mateusz1913/dishdiary"));
                // Load css files from data/resources/ directory
                application.setup_css();

                let window = DishdiaryWindow::new(application);
                window.upcast()
            };

            // Ask the window manager/compositor to present the window
            window.present();
        }
    }

    impl GtkApplicationImpl for DishdiaryApplication {}
    impl AdwApplicationImpl for DishdiaryApplication {}
}

glib::wrapper! {
    pub struct DishdiaryApplication(ObjectSubclass<imp::DishdiaryApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl DishdiaryApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::new(&[("application-id", &application_id), ("flags", flags)])
            .expect("Failed to create DishdiaryApplication")
    }

    fn setup_gactions(&self) {
        let quit_action = gio::SimpleAction::new("quit", None);
        quit_action.connect_activate(clone!(@weak self as app => move |_, _| {
            app.quit();
        }));
        self.add_action(&quit_action);

        let about_action = gio::SimpleAction::new("about", None);
        about_action.connect_activate(clone!(@weak self as app => move |_, _| {
            app.show_about();
        }));
        self.add_action(&about_action);
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let about_window = adw::AboutWindow::builder()
            .application_name("Dish Diary")
            // .application_icon(&APP_ID.to_onwed())
            .developer_name("Mateusz Mędrek")
            .developers(vec!["Mateusz Mędrek".into()])
            .version(VERSION)
            // .website("")
            .build();
        about_window.set_transient_for(Some(&window));
        about_window.set_modal(true);
        about_window.show();
    }

    fn setup_css(&self) {
        let provider = gtk::CssProvider::new();
        provider.load_from_resource("/dev/mateusz1913/dishdiary/style.css");

        if let Some(display) = gdk::Display::default() {
            gtk::StyleContext::add_provider_for_display(
                &display,
                &provider,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );
        }
    }
}

