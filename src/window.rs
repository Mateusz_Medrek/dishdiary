use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate};

use crate::all_dishes_list_row::AllDishesListRow;
use crate::all_dishes_page::AllDishesPage;
use crate::database::{get_all_dishes, remove_dish};
use crate::dish_details_page::DishDetailsPage;
use crate::dish_object::DishObject;
use crate::main_stack_header_bar::MainStackHeaderBar;
use crate::randomized_dishes_page::RandomizedDishesPage;
use crate::randomizing_spinner::RandomizingSpinner;
use crate::welcome_page::WelcomePage;

mod imp {
    use super::*;

    #[derive(Default, CompositeTemplate)]
    #[template(resource = "/dev/mateusz1913/dishdiary/ui/window.ui")]
    pub struct DishdiaryWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<MainStackHeaderBar>,
        #[template_child]
        pub main_stack: TemplateChild<adw::ViewStack>,
        #[template_child]
        pub root_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub dish_details_page: TemplateChild<DishDetailsPage>,
        #[template_child]
        pub new_dish_page: TemplateChild<DishDetailsPage>,
        #[template_child]
        pub welcome_stack: TemplateChild<adw::ViewStack>,
        #[template_child]
        pub randomizing_spinner: TemplateChild<RandomizingSpinner>,
        #[template_child]
        pub randomized_dishes: TemplateChild<RandomizedDishesPage>,
        #[template_child]
        pub welcome_page: TemplateChild<WelcomePage>,
        #[template_child]
        pub all_dishes_page: TemplateChild<AllDishesPage>,
        #[template_child]
        pub view_switcher_bar: TemplateChild<adw::ViewSwitcherBar>,
        // All dishes list
        pub all_dishes_list_model: std::cell::RefCell<Option<gio::ListStore>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DishdiaryWindow {
        const NAME: &'static str = "DishdiaryWindow";
        type Type = super::DishdiaryWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for DishdiaryWindow {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            self.set_adw_color_scheme();
            obj.set_shortcuts();
            self.setup_stack_switcher();
            self.setup_primary_menu();

            obj.setup_all_dishes_list();
        }
    }

    impl WidgetImpl for DishdiaryWindow {}
    impl WindowImpl for DishdiaryWindow {}
    impl ApplicationWindowImpl for DishdiaryWindow {}
    impl AdwApplicationWindowImpl for DishdiaryWindow {}

    impl DishdiaryWindow {
        fn set_adw_color_scheme(&self) {
            let style_manager = adw::StyleManager::default();
            style_manager.set_color_scheme(adw::ColorScheme::Default);
        }

        fn setup_stack_switcher(&self) {
            self.header_bar
                .imp()
                .stack_switcher
                .set_stack(Some(&self.main_stack.get()));
        }

        fn setup_primary_menu(&self) {
            // Manually load primary menu
            let primary_menu_builder =
                gtk::Builder::from_resource("/dev/mateusz1913/dishdiary/ui/primary_menu.ui");
            // `primary_menu` is an id of primary menu template
            let primary_menu: gio::MenuModel = primary_menu_builder.object("primary_menu").unwrap();
            self.header_bar
                .imp()
                .menu_button
                .set_menu_model(Some(&primary_menu));
        }
    }
}

glib::wrapper! {
    pub struct DishdiaryWindow(ObjectSubclass<imp::DishdiaryWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

#[gtk::template_callbacks]
impl DishdiaryWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::new(&[("application", application)])
            .expect("Failed to create DishdiaryWindow")
    }

    fn set_shortcuts(&self) {
        // Manually load shortcuts overlay
        let shortcuts_builder =
            gtk::Builder::from_resource("/dev/mateusz1913/dishdiary/ui/shortcuts.ui");
        // `help_overlay` is an id of shortcuts template
        let shortcuts = shortcuts_builder.object("help_overlay").unwrap();
        self.set_help_overlay(Some(&shortcuts));
    }

    fn get_all_dishes_list_model(&self) -> gio::ListStore {
        self.imp()
            .all_dishes_list_model
            .borrow()
            .clone()
            .expect("Failed to get current all dishes list")
    }

    pub fn get_all_dishes_from_db(&self) {
        let model = gio::ListStore::new(DishObject::static_type());

        let all_dishes_result = get_all_dishes();
        match all_dishes_result {
            Ok(dishes) => {
                let mut iter = dishes.into_iter();
                while let Some(dish) = iter.next() {
                    model.append(&DishObject::new(dish.id, dish.title, dish.description));
                }
            }
            Err(_) => {
                println!("OKRUTNIK");
            }
        }

        self.imp().all_dishes_list_model.replace(Some(model));
        let selection_model = gtk::NoSelection::new(Some(&self.get_all_dishes_list_model()));
        self.imp()
            .all_dishes_page
            .set_list_model(Some(&selection_model));
    }

    fn setup_all_dishes_list(&self) {
        self.get_all_dishes_from_db();

        let factory = gtk::SignalListItemFactory::new();
        factory.connect_setup(glib::clone!(@weak self as app => move |_, list_item| {
            app.handle_list_item_setup(list_item);
        }));
        factory.connect_bind(glib::clone!(@weak self as app => move |_, list_item| {
            app.handle_list_item_bind(list_item);
        }));
        factory.connect_unbind(glib::clone!(@weak self as app => move |_, list_item| {
            app.handle_list_item_unbind(list_item);
        }));
        self.imp().all_dishes_page.set_list_factory(Some(&factory));
    }

    fn handle_list_item_setup(&self, list_item: &gtk::ListItem) {
        let all_dishes_list_row = AllDishesListRow::new();
        list_item.set_child(Some(&all_dishes_list_row));
        all_dishes_list_row.connect_clicked(
            glib::clone!(@weak self as app, @weak list_item as weak_list_item => move |_| {
                // do sth with list_item
                let dish_object = weak_list_item
                    .item()
                    .expect("Item has to exist")
                    .downcast::<DishObject>()
                    .expect("Item has to be a DishObject");
                // navigate to dish_details with dish object;
                app.imp().dish_details_page.set_dish_object(Some(dish_object));
                app.imp().root_stack.set_visible_child_name("dish_details");
            }),
        );
    }

    fn handle_list_item_bind(&self, list_item: &gtk::ListItem) {
        let dish_object = list_item
            .item()
            .expect("Item has to exist")
            .downcast::<DishObject>()
            .expect("Item has to be a DishObject");

        let all_dishes_list_row = list_item
            .child()
            .expect("Child has to exist")
            .downcast::<AllDishesListRow>()
            .expect("Child has to be an AllDishesListRow");

        all_dishes_list_row.bind(&dish_object);
    }

    fn handle_list_item_unbind(&self, list_item: &gtk::ListItem) {
        let all_dishes_list_row = list_item
            .child()
            .expect("Child has to exist")
            .downcast::<AllDishesListRow>()
            .expect("Child has to be an AllDishesListRow");

        all_dishes_list_row.unbind_all();
    }

    #[template_callback]
    pub fn handle_navigate_back_from_dish_details_page(&self, _button: &gtk::Button) {
        self.imp().root_stack.set_visible_child_name("main_stack");
    }

    #[template_callback]
    pub fn handle_remove_dish(&self, _button: &gtk::Button, dish_object: &DishObject) {
        let dialog = adw::MessageDialog::builder()
            .heading("Delete dish")
            .body("Do you want to delete dish?")
            .modal(true)
            .transient_for(self)
            .build();

        dialog.add_response("cancel", "Cancel");
        dialog.add_response("delete", "Delete");
        dialog.set_response_appearance("cancel", adw::ResponseAppearance::Default);
        dialog.set_response_appearance("delete", adw::ResponseAppearance::Destructive);
        dialog.set_close_response("cancel");
        dialog.set_default_response(Some("cancel"));

        dialog.connect_response(
            None,
            glib::clone!(@weak self as app, @weak dish_object => move |_, response| {
                if response == "delete" {
                    remove_dish(dish_object.property::<i32>("id")).unwrap();
                    app.get_all_dishes_from_db();
                    app.imp().root_stack.set_visible_child_name("main_stack");
                }
            }),
        );

        dialog.present();
    }

    #[template_callback]
    pub fn handle_submit_dish_details_page(&self, _button: &gtk::Button) {
        self.get_all_dishes_from_db();
        self.imp().root_stack.set_visible_child_name("main_stack");
    }

    #[template_callback]
    pub fn handle_next_day_button_clicked(&self, _button: &gtk::Button) {
        self.imp().welcome_stack.set_visible_child_name("spinner");
        glib::timeout_add_seconds_local(
            3,
            glib::clone!(@weak self as app => @default-return glib::Continue(false), move || {
                app.imp().welcome_stack.set_visible_child_name("welcome");
                glib::Continue(false)
            }),
        );
    }

    #[template_callback]
    pub fn handle_next_three_days_button_clicked(&self, _button: &gtk::Button) {
        self.imp().welcome_stack.set_visible_child_name("results");
    }

    #[template_callback]
    pub fn handle_navigate_back_from_results_page(&self, _button: &gtk::Button) {
        self.imp().welcome_stack.set_visible_child_name("welcome");
    }

    #[template_callback]
    pub fn handle_view_switcher_title_visible(&self, view_switcher: &adw::ViewSwitcherTitle) {
        // Will be visible ONLY if there are at least 3 pages in adw::ViewStack;
        self.imp()
            .view_switcher_bar
            .set_reveal(view_switcher.is_title_visible());
    }

    #[template_callback]
    pub fn handle_add_new_dish_button_clicked(&self, _button: &gtk::Button) {
        self.imp().new_dish_page.set_dish_object(None);
        self.imp().root_stack.set_visible_child_name("new_dish");
    }
}

