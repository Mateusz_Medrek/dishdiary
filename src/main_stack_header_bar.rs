use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use adw::subclass::prelude::*;

use once_cell::sync::Lazy;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/dev/mateusz1913/dishdiary/ui/main_stack_header_bar.ui")]
    pub struct MainStackHeaderBar {
        #[template_child]
        pub header_bar: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub add_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub menu_button: TemplateChild<gtk::MenuButton>,
        #[template_child]
        pub stack_switcher: TemplateChild<adw::ViewSwitcherTitle>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MainStackHeaderBar {
        const NAME: &'static str = "MainStackHeaderBar";
        type Type = super::MainStackHeaderBar;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for MainStackHeaderBar {
        fn signals() -> &'static [glib::subclass::Signal] {
            static SIGNALS: Lazy<Vec<glib::subclass::Signal>> = Lazy::new(|| {
                vec![
                    glib::subclass::Signal::builder(
                        "add-button-clicked",
                        &[gtk::Button::static_type().into()],
                        <()>::static_type().into(),
                    ).build(),
                    glib::subclass::Signal::builder(
                        "view-switcher-title-visible",
                        &[adw::ViewSwitcherTitle::static_type().into()],
                        <()>::static_type().into(),
                    ).build(),
                ]
            });

            SIGNALS.as_ref()
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            self.add_button.connect_clicked(glib::clone!(@weak obj as app => move |button| {
                app.handle_button_clicked(button);
            }));
        }
    }

    impl WidgetImpl for MainStackHeaderBar {}

    impl BinImpl for MainStackHeaderBar {}
}

glib::wrapper! {
    pub struct MainStackHeaderBar(ObjectSubclass<imp::MainStackHeaderBar>)
        @extends gtk::Widget, adw::Bin,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

#[gtk::template_callbacks]
impl MainStackHeaderBar {
    pub fn new() -> Self {
        glib::Object::new(&[])
            .expect("Failed to create MainStackHeaderBar")
    }

    pub fn handle_button_clicked(&self, button: &gtk::Button) {
        self.emit_by_name::<()>("add-button-clicked", &[&button]);
    }

    #[template_callback]
    pub fn on_view_switcher_title_visible(&self, _param: glib::ParamSpec, view_switcher: &adw::ViewSwitcherTitle) {
        self.emit_by_name::<()>("view-switcher-title-visible", &[&view_switcher]);
    }
}

