// All of this is based on setup from Shortwave app,
// but with migration changes from diesel 1.4 to diesel 2.0
// https://gitlab.gnome.org/World/Shortwave/-/blob/222f252dcd5c79b278a03b83700d15ca0835b24e/src/database/connection.rs

use diesel::prelude::*;
use diesel::r2d2;
use diesel::r2d2::ConnectionManager;
use diesel::sqlite::Sqlite;
use gtk::glib;
use once_cell::sync::Lazy;

use crate::config::NAME;

pub const MIGRATIONS: diesel_migrations::EmbeddedMigrations =
    diesel_migrations::embed_migrations!("./migrations");

pub static DB_PATH: Lazy<std::path::PathBuf> = Lazy::new(|| {
    let mut path = glib::user_data_dir();
    path.push(NAME);
    path.push("database.db");
    path
});

fn run_migrations(
    connection: &mut impl diesel_migrations::MigrationHarness<Sqlite>,
) -> Result<
    Vec<diesel::migration::MigrationVersion>,
    Box<dyn std::error::Error + Send + Sync + 'static>,
> {
    connection.run_pending_migrations(MIGRATIONS)
}

fn init_connection_pool(db_path: &str) -> Pool {
    let manager = ConnectionManager::<SqliteConnection>::new(db_path);
    let pool = r2d2::Pool::builder()
        .max_size(1)
        .build(manager)
        .expect(format!("Failed to create pool with db: {}", db_path).as_str());

    let mut db = pool
        .get()
        .expect(format!("Failed to initialze pool with db: {}", db_path).as_str());
    run_migrations(&mut db).expect(format!("Failed to run migrations on db: {}", db_path).as_str());

    pool
}

type Pool = r2d2::Pool<ConnectionManager<SqliteConnection>>;

static POOL: Lazy<Pool> = Lazy::new(|| init_connection_pool(DB_PATH.to_str().unwrap()));

pub fn connection() -> Pool {
    POOL.clone()
}

