use diesel::prelude::*;

use super::models::{Dish, NewDish};
use super::schema::dishes;
use super::schema::dishes::dsl::*;
use crate::database::connection;

pub fn get_all_dishes() -> Result<Vec<Dish>, diesel::result::Error> {
    let mut db_conn = connection::connection().get().unwrap();
    let saved_dishes = dishes::table.load::<Dish>(&mut db_conn)?;
    Ok(saved_dishes)
}

pub fn create_dish(new_dish: NewDish) -> Result<Dish, diesel::result::Error> {
    let mut db_conn = connection::connection().get().unwrap();
    let created_dish = diesel::insert_into(dishes::table)
        .values((
            title.eq(new_dish.title),
            description.eq(new_dish.description),
        ))
        .get_result::<Dish>(&mut *db_conn)?;
    Ok(created_dish)
}

pub fn update_dish(dish: Dish) -> Result<Dish, diesel::result::Error> {
    let mut db_conn = connection::connection().get().unwrap();
    let updated_dish = diesel::replace_into(dishes::table)
        .values(dish)
        .get_result(&mut *db_conn)?;
    Ok(updated_dish)
}

pub fn remove_dish(dish_id: i32) -> Result<(), diesel::result::Error> {
    let mut db_conn = connection::connection().get().unwrap();
    diesel::delete(dishes::table.filter(id.eq(dish_id))).execute(&mut *db_conn)?;
    Ok(())
}

