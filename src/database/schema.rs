// @generated automatically by Diesel CLI.

diesel::table! {
    dishes (id) {
        id -> Integer,
        title -> Text,
        description -> Text,
    }
}
