use std::fmt::Display;

use diesel::prelude::*;

use super::schema::*;

#[derive(Clone, Default, Debug, Insertable, Queryable)]
#[diesel(table_name = dishes)]
pub struct Dish {
    pub id: i32,
    pub title: String,
    pub description: String,
}

impl Display for Dish {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Dish(id: {}, title: {}, description: {})",
            self.id, self.title, self.description
        )
    }
}

#[derive(Insertable)]
#[diesel(table_name = dishes)]
pub struct NewDish {
    pub title: String,
    pub description: String,
}

