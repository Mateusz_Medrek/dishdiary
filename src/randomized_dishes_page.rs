use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

use once_cell::sync::Lazy;

use crate::database::get_all_dishes;
use crate::dish_object::DishObject;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/dev/mateusz1913/dishdiary/ui/randomized_dishes_page.ui")]
    pub struct RandomizedDishesPage {
        #[template_child]
        pub back_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub list_box: TemplateChild<gtk::ListBox>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for RandomizedDishesPage {
        const NAME: &'static str = "RandomizedDishesPage";
        type Type = super::RandomizedDishesPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for RandomizedDishesPage {
        fn signals() -> &'static [glib::subclass::Signal] {
            static SIGNALS: Lazy<Vec<glib::subclass::Signal>> = Lazy::new(|| {
                vec![glib::subclass::Signal::builder(
                    "back-button-clicked",
                    &[gtk::Button::static_type().into()],
                    <()>::static_type().into(),
                )
                .build()]
            });

            SIGNALS.as_ref()
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            self.back_button
                .connect_clicked(glib::clone!(@weak obj as app => move |button| {
                    app.back_button_clicked(button);
                }));

            let row = adw::ExpanderRow::builder().title("Test").build();
            let buffer = gtk::TextBuffer::builder()
                .text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum neque egestas congue quisque egestas diam in. Nibh venenatis cras sed felis eget velit aliquet sagittis id. Nisl condimentum id venenatis a condimentum vitae. Augue interdum velit euismod in pellentesque massa placerat duis ultricies. Duis tristique sollicitudin nibh sit amet commodo nulla facilisi. Consectetur libero id faucibus nisl tincidunt eget. In metus vulputate eu scelerisque felis.")
                .build();
            let text_view = gtk::TextView::with_buffer(&buffer);
            text_view.set_wrap_mode(gtk::WrapMode::WordChar);
            text_view.set_editable(false);
            text_view.set_hexpand(true);
            text_view.add_css_class("description-result");
            row.add_row(&text_view);
            self.list_box.get().append(&row);
            let row_two = adw::ExpanderRow::builder().title("Test2").build();
            let buffer_two = gtk::TextBuffer::builder()
                .text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum neque egestas congue quisque egestas diam in. Nibh venenatis cras sed felis eget velit aliquet sagittis id. Nisl condimentum id venenatis a condimentum vitae. Augue interdum velit euismod in pellentesque massa placerat duis ultricies. Duis tristique sollicitudin nibh sit amet commodo nulla facilisi. Consectetur libero id faucibus nisl tincidunt eget. In metus vulputate eu scelerisque felis. Etiam erat velit scelerisque in dictum non consectetur a erat. Ridiculus mus mauris vitae ultricies leo integer. Egestas sed tempus urna et pharetra pharetra massa massa. Id diam vel quam elementum pulvinar etiam. Id leo in vitae turpis massa sed elementum. Donec ac odio tempor orci.")
                .build();
            let text_view_two = gtk::TextView::with_buffer(&buffer_two);
            text_view_two.set_wrap_mode(gtk::WrapMode::WordChar);
            text_view_two.set_editable(false);
            text_view_two.set_hexpand(true);
            text_view_two.add_css_class("description-result");
            row_two.add_row(&text_view_two);
            self.list_box.get().append(&row_two);
        }
    }

    impl WidgetImpl for RandomizedDishesPage {}

    impl BoxImpl for RandomizedDishesPage {}
}

glib::wrapper! {
    pub struct RandomizedDishesPage(ObjectSubclass<imp::RandomizedDishesPage>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl RandomizedDishesPage {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create RandomizedDishesPage")
    }

    pub fn back_button_clicked(&self, back_button: &gtk::Button) {
        self.emit_by_name::<()>("back-button-clicked", &[&back_button]);
    }

    pub fn rand_three_dishes(&self) {
        let all_dishes_result = get_all_dishes();
        match all_dishes_result {
            Ok(dishes) => {
                let dishes_count = dishes.len();

                // TODO: randomize 3 dishes here

                let mut iter = dishes.into_iter();
                while let Some(dish) = iter.next() {
                    // model.append(&DishObject::new(dish.id, dish.title, dish.description));
                }
            }
            Err(_) => {
                println!("OKRUTNIK");
            }
        }
    }
}

