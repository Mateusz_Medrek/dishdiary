use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

use crate::dish_object::DishObject;

mod imp {
    use super::*;

    #[derive(Default, CompositeTemplate)]
    #[template(resource = "/dev/mateusz1913/dishdiary/ui/all_dishes_list_row.ui")]
    pub struct AllDishesListRow {
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
        // bindings
        pub bindings: std::cell::RefCell<Vec<glib::Binding>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AllDishesListRow {
        const NAME: &'static str = "AllDishesListRow";
        type Type = super::AllDishesListRow;
        type ParentType = gtk::Button;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AllDishesListRow {}

    impl WidgetImpl for AllDishesListRow {}

    impl ButtonImpl for AllDishesListRow {}
}

glib::wrapper! {
    pub struct AllDishesListRow(ObjectSubclass<imp::AllDishesListRow>)
        @extends gtk::Widget, gtk::Button,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Actionable;
}

impl AllDishesListRow {
    pub fn new() -> Self {
        glib::Object::new(&[])
            .expect("Failed to create AllDishesListRow")
    }

    pub fn bind(&self, dish_object: &DishObject) {
        let mut bindings = self.imp().bindings.borrow_mut();
        let label = self.imp().label.get();

        let label_binding = dish_object
            .bind_property("title", &label, "label")
            .flags(glib::BindingFlags::SYNC_CREATE)
            .build();

        bindings.push(label_binding);
    }

    pub fn unbind_all(&self) {
        for binding in self.imp().bindings.borrow_mut().drain(..) {
            binding.unbind();
        }
    }
}

