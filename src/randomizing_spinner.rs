use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/dev/mateusz1913/dishdiary/ui/randomizing_spinner.ui")]
    pub struct RandomizingSpinner {
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
        #[template_child]
        pub spinner: TemplateChild<gtk::Spinner>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for RandomizingSpinner {
        const NAME: &'static str = "RandomizingSpinner";
        type Type = super::RandomizingSpinner;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for RandomizingSpinner {}

    impl WidgetImpl for RandomizingSpinner {}

    impl BoxImpl for RandomizingSpinner {}
}

glib::wrapper! {
    pub struct RandomizingSpinner(ObjectSubclass<imp::RandomizingSpinner>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl RandomizingSpinner {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create RandomizingSpinner")
    }
}

