use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/dev/mateusz1913/dishdiary/ui/all_dishes_page.ui")]
    pub struct AllDishesPage {
        #[template_child]
        pub all_dishes_list: TemplateChild<gtk::ListView>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AllDishesPage {
        const NAME: &'static str = "AllDishesPage";
        type Type = super::AllDishesPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AllDishesPage {}

    impl WidgetImpl for AllDishesPage {}

    impl BoxImpl for AllDishesPage {}
}

glib::wrapper! {
    pub struct AllDishesPage(ObjectSubclass<imp::AllDishesPage>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl AllDishesPage {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create AllDishesPage")
    }

    pub fn set_list_model(&self, model: Option<&impl IsA<gtk::SelectionModel>>) {
        self.imp().all_dishes_list.set_model(model);
    }

    pub fn set_list_factory(&self, factory: Option<&impl IsA<gtk::ListItemFactory>>) {
        self.imp().all_dishes_list.set_factory(factory);
    }
}

