use glib::prelude::*;
use glib::subclass::prelude::*;
use gtk::glib;

use once_cell::sync::Lazy;

use crate::database::{create_dish, update_dish, Dish, NewDish};

mod imp {
    use super::*;

    #[derive(Default)]
    pub struct DishObject {
        pub data: std::rc::Rc<std::cell::RefCell<Dish>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DishObject {
        const NAME: &'static str = "DishObject";
        type Type = super::DishObject;
    }

    impl ObjectImpl for DishObject {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![
                    glib::ParamSpecInt::builder("id")
                        .blurb("Dish's id")
                        .flags(glib::ParamFlags::READWRITE)
                        .build(),
                    glib::ParamSpecString::builder("title")
                        .blurb("Dish's title")
                        .flags(glib::ParamFlags::READWRITE)
                        .build(),
                    glib::ParamSpecString::builder("description")
                        .blurb("Dish's description")
                        .flags(glib::ParamFlags::READWRITE)
                        .build(),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            _obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "id" => {
                    let id = value.get().expect("The value of `id` must be of type int");
                    self.data.borrow_mut().id = id;
                }
                "title" => {
                    let title = value
                        .get()
                        .expect("The value of `title` must be of type string");
                    self.data.borrow_mut().title = title;
                }
                "description" => {
                    let description = value
                        .get()
                        .expect("The value of `description` must be of type string");
                    self.data.borrow_mut().description = description;
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "id" => self.data.borrow().id.to_value(),
                "title" => self.data.borrow().title.to_value(),
                "description" => self.data.borrow().description.to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct DishObject(ObjectSubclass<imp::DishObject>);
}

impl DishObject {
    pub fn new(id: i32, title: String, description: String) -> Self {
        glib::Object::new(&[
            ("id", &id),
            ("title", &title),
            ("description", &description),
        ])
        .expect("Failed to create DishObject")
    }

    pub fn create(title: &str, description: &str) -> Self {
        let new_dish = NewDish {
            title: title.to_string(),
            description: description.to_string(),
        };
        let created_dish = create_dish(new_dish).expect(
            format!(
                "Failed to create dish in db - title: {}, description: {}",
                title, description
            )
            .as_str(),
        );
        DishObject::new(
            created_dish.id,
            created_dish.title,
            created_dish.description,
        )
    }

    pub fn update(updated_dish: Dish) {
        update_dish(updated_dish.clone())
            .expect(format!("Failed to update dish in db: {}", updated_dish).as_str());
    }
}

