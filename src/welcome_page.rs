use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

use once_cell::sync::Lazy;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/dev/mateusz1913/dishdiary/ui/welcome_page.ui")]
    pub struct WelcomePage {
        #[template_child]
        pub rand_next_day: TemplateChild<gtk::Button>,
        #[template_child]
        pub rand_next_three_days: TemplateChild<gtk::Button>,
        #[template_child]
        pub revealer: TemplateChild<gtk::Revealer>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WelcomePage {
        const NAME: &'static str = "WelcomePage";
        type Type = super::WelcomePage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for WelcomePage {
        fn signals() -> &'static [glib::subclass::Signal] {
            static SIGNALS: Lazy<Vec<glib::subclass::Signal>> = Lazy::new(|| {
                vec![
                    glib::subclass::Signal::builder(
                        "rand-next-day-button-clicked",
                        &[gtk::Button::static_type().into()],
                        <()>::static_type().into(),
                    ).build(),
                    glib::subclass::Signal::builder(
                        "rand-next-three-days-button-clicked",
                        &[gtk::Button::static_type().into()],
                        <()>::static_type().into(),
                    ).build()
                ]
            });

            SIGNALS.as_ref()
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            self.rand_next_day.connect_clicked(glib::clone!(@weak obj as app => move |button| {
                app.rand_next_day_button_clicked(button);
            }));

            self.rand_next_three_days.connect_clicked(glib::clone!(@weak obj as app => move |button| {
                app.rand_next_three_days_button_clicked(button);
            }));
        }
    }

    impl WidgetImpl for WelcomePage {}

    impl BoxImpl for WelcomePage {}
}

glib::wrapper! {
    pub struct WelcomePage(ObjectSubclass<imp::WelcomePage>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl WelcomePage {
    pub fn new() -> Self {
        glib::Object::new(&[])
            .expect("Failed to create WelcomePage")
    }

    pub fn rand_next_day_button_clicked(&self, button: &gtk::Button) {
        self.emit_by_name::<()>("rand-next-day-button-clicked", &[&button]);
    }

    pub fn rand_next_three_days_button_clicked(&self, button: &gtk::Button) {
        self.reveal_spinner();
        self.emit_by_name::<()>("rand-next-three-days-button-clicked", &[&button]);
    }

    pub fn reveal_spinner(&self) {
        self.imp().revealer.set_reveal_child(true);
        glib::timeout_add_seconds_local(3, glib::clone!(@weak self as app => @default-return glib::Continue(false), move || {
            app.imp().revealer.set_reveal_child(false);
            glib::Continue(false)
        }));
    }
}

