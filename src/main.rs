mod all_dishes_list_row;
mod all_dishes_page;
mod application;
mod config;
mod database;
mod dish_details_page;
mod dish_object;
mod main_stack_header_bar;
mod randomized_dishes_page;
mod randomizing_spinner;
mod welcome_page;
mod window;

use self::application::DishdiaryApplication;
use self::window::DishdiaryWindow;

use config::{APP_ID, GETTEXT_PACKAGE, LOCALEDIR, NAME, PKGDATADIR};
use gettextrs::{bind_textdomain_codeset, bindtextdomain, textdomain};
use gtk::prelude::*;
use gtk::{gio, glib};

fn main() {
    // Handle creating directory for DB;
    let mut path = glib::user_data_dir();
    path.push(NAME);
    std::fs::create_dir_all(&path).unwrap();

    // Set up gettext translations
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Unable to bind the text domain");
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8")
        .expect("Unable to set the text domain encoding");
    textdomain(GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    // Load resources
    let resources = gio::Resource::load(PKGDATADIR.to_owned() + "/dishdiary.gresource")
        .expect("Could not load resources");
    gio::resources_register(&resources);

    glib::set_application_name(NAME);

    // Create a new GtkApplication. The application manages our main loop,
    // application windows, integration with the window manager/compositor, and
    // desktop features such as file opening and single-instance applications.
    let app = DishdiaryApplication::new(&APP_ID.to_owned(), &gio::ApplicationFlags::empty());

    // Run the application. This function will block until the application
    // exits. Upon return, we have our exit code to return to the shell. (This
    // is the code you see when you do `echo $?` after running a command in a
    // terminal.
    std::process::exit(app.run());
}

