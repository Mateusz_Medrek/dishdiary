use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

use once_cell::sync::Lazy;

use crate::database::Dish;
use crate::dish_object::DishObject;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/dev/mateusz1913/dishdiary/ui/dish_details_page.ui")]
    pub struct DishDetailsPage {
        #[template_child]
        pub submit_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub delete_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub back_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub title_entry: TemplateChild<gtk::Entry>,
        #[template_child]
        pub description_entry: TemplateChild<gtk::TextView>,
        #[template_child]
        pub window_title: TemplateChild<adw::WindowTitle>,
        // Dish object
        pub dish_object: std::cell::RefCell<Option<DishObject>>,
        pub bindings: std::cell::RefCell<Vec<glib::Binding>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DishDetailsPage {
        const NAME: &'static str = "DishDetailsPage";
        type Type = super::DishDetailsPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for DishDetailsPage {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpecBoolean::new(
                    "show-back-button",
                    "show-back-button",
                    "Shows the back button in the header bar",
                    false,
                    glib::ParamFlags::READWRITE,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            _obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "show-back-button" => {
                    let bool_value = value.get().expect("The value needs to be of type `bool`");
                    self.back_button.set_visible(bool_value);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "show-back-button" => self.back_button.is_visible().to_value(),
                _ => unimplemented!(),
            }
        }

        fn signals() -> &'static [glib::subclass::Signal] {
            static SIGNALS: Lazy<Vec<glib::subclass::Signal>> = Lazy::new(|| {
                vec![
                    glib::subclass::Signal::builder(
                        "back-button-clicked",
                        &[gtk::Button::static_type().into()],
                        <()>::static_type().into(),
                    )
                    .build(),
                    glib::subclass::Signal::builder(
                        "delete-button-clicked",
                        &[
                            gtk::Button::static_type().into(),
                            DishObject::static_type().into(),
                        ],
                        <()>::static_type().into(),
                    )
                    .build(),
                    glib::subclass::Signal::builder(
                        "submit-button-clicked",
                        &[gtk::Button::static_type().into()],
                        <()>::static_type().into(),
                    )
                    .build(),
                ]
            });

            SIGNALS.as_ref()
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            self.back_button
                .connect_clicked(glib::clone!(@weak obj as app => move |button| {
                    app.back_button_clicked(button);
                }));

            self.delete_button
                .connect_clicked(glib::clone!(@weak obj as app => move |button| {
                    app.delete_button_clicked(button);
                }));

            self.submit_button
                .connect_clicked(glib::clone!(@weak obj as app => move |button| {
                    app.submit_button_clicked(button);
                }));
        }
    }

    impl WidgetImpl for DishDetailsPage {}
    impl BoxImpl for DishDetailsPage {}
}

glib::wrapper! {
    pub struct DishDetailsPage(ObjectSubclass<imp::DishDetailsPage>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl DishDetailsPage {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create DishDetailsPage")
    }

    pub fn back_button_clicked(&self, back_button: &gtk::Button) {
        self.emit_by_name::<()>("back-button-clicked", &[&back_button]);
    }

    pub fn delete_button_clicked(&self, delete_button: &gtk::Button) {
        self.emit_by_name::<()>(
            "delete-button-clicked",
            &[
                &delete_button,
                &self.imp().dish_object.borrow().as_ref().unwrap(),
            ],
        );
    }

    pub fn submit_button_clicked(&self, submit_button: &gtk::Button) {
        let title = self.imp().title_entry.get().buffer().text();
        let description_buffer = self.imp().description_entry.get().buffer();
        let description = description_buffer.text(
            &description_buffer.start_iter(),
            &description_buffer.end_iter(),
            false,
        );

        if let Some(saved_dish_object) = self.imp().dish_object.borrow().as_ref() {
            DishObject::update(Dish {
                id: saved_dish_object.property::<i32>("id"),
                title,
                description: description.to_string(),
            });
        } else {
            DishObject::create(&title, &description);
        }
        self.emit_by_name::<()>("submit-button-clicked", &[&submit_button]);

        self.imp().dish_object.replace(None);
        self.clear_form();
    }

    pub fn set_dish_object(&self, dish_object: Option<DishObject>) {
        if dish_object.is_none() {
            self.imp().dish_object.replace(None);
            self.clear_form();
            return;
        }
        self.imp().submit_button.set_label("Edit");
        self.imp().delete_button.set_visible(true);

        let new_dish_object = dish_object.unwrap().clone();
        self.imp().dish_object.replace(Some(new_dish_object));

        let mut bindings = self.imp().bindings.borrow_mut();
        for binding in bindings.drain(..) {
            binding.unbind();
        }

        if let Some(saved_dish_object) = self.imp().dish_object.borrow().as_ref() {
            // Bind new dish_object;
            let title_binding = saved_dish_object
                .bind_property("title", &self.imp().title_entry.get().buffer(), "text")
                .flags(glib::BindingFlags::SYNC_CREATE)
                .build();

            bindings.push(title_binding);

            let window_title_binding = saved_dish_object
                .bind_property("title", &self.imp().window_title.get(), "title")
                .flags(glib::BindingFlags::SYNC_CREATE)
                .build();

            bindings.push(window_title_binding);

            let description_binding = saved_dish_object
                .bind_property(
                    "description",
                    &self.imp().description_entry.get().buffer(),
                    "text",
                )
                .flags(glib::BindingFlags::SYNC_CREATE)
                .build();

            bindings.push(description_binding);
        }
    }

    fn clear_form(&self) {
        let mut bindings = self.imp().bindings.borrow_mut();
        for binding in bindings.drain(..) {
            binding.unbind();
        }

        self.imp().title_entry.get().buffer().set_text("");
        self.imp().description_entry.get().buffer().set_text("");
    }
}

